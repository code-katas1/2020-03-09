﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculator1
{
    public class Calculator
    {
        private readonly string _customDelimiterId = "//";
        private readonly string _newLine = "\n";

        public int Add(string numbers)
        {
            if (string.IsNullOrWhiteSpace(numbers))
            {
                return 0;
            }

            var delimiters = GetDelimiters(numbers);
            var numbersArray = GetNumbers(delimiters, numbers);

            ValidateNumbers(numbersArray);

            return GetSum(numbersArray);
        }

        private string[] GetDelimiters(string numbers)
        {
            var multipleDelimiterStartIdentifier = $"{_customDelimiterId}[";
            var multipleDelimiterEndIdentifier = $"]{_newLine}";
            var customDelimiterSeperator = "][";

            string delimiter;

            if (!numbers.Contains(_customDelimiterId))
            {
                return new[] { _newLine, "," };
            }
            else if (numbers.StartsWith(multipleDelimiterStartIdentifier) && numbers.Contains(multipleDelimiterEndIdentifier))
            {
                delimiter = numbers.Substring(numbers.IndexOf(multipleDelimiterStartIdentifier) + 2, numbers.IndexOf(multipleDelimiterEndIdentifier) - 1);
                delimiter = delimiter.Remove(delimiter.LastIndexOf("]"), 1).Remove(0, 1);

                return delimiter.Split(new[] { customDelimiterSeperator }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            }

            delimiter = numbers.Substring(numbers.IndexOf(_customDelimiterId) + 2, numbers.IndexOf(_newLine) - 2);

            return new[] { delimiter };
        }

        public string RemoveDelimiterSection(string numbers)
        {
            if (numbers.StartsWith(_customDelimiterId))
            {
                return numbers.Substring(numbers.IndexOf(_newLine) + 1);
            }

            return numbers;
        }

        private int[] GetNumbers(string[] delimiters, string numbers)
        {
            try
            {
                var numbersOnly = RemoveDelimiterSection(numbers);
                var numbersArray = numbersOnly.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                return numbersArray.Select(int.Parse).ToArray();
            }
            catch (Exception)
            {
                throw new Exception("Bad string");
            }
        }

        private void ValidateNumbers(int[] numbersArray)
        {
            List<int> negativeNumbers = new List<int>();

            foreach (var negatives in numbersArray)
            {
                if (negatives < 0)
                {
                    negativeNumbers.Add(negatives);
                }
            }

            if (negativeNumbers.Count != 0)
            {
                throw new Exception($"Negatives not allowed {string.Join(", ", negativeNumbers)}");
            }
        }

        private int GetSum(int[] numbersArray)
        {
            var sum = numbersArray[0];

            for (var i = 1; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] < 1001)
                {
                    sum += numbersArray[i];
                }
            }

            return sum;
        }
    }
}
