﻿using System;
using NUnit.Framework;

namespace StringCalculator1.Tests
{
    [TestFixture]
    public class CalculatorTest
    {
        private Calculator _calculator;

        [OneTimeSetUp]
        public void Initialize()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void Given_EmptyString_When_Adding_Then_ReturnZero()
        {
            var actual = _calculator.Add("");
            var expected = 0;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_OneNumber_When_Adding_Then_ReturnThatNumber()
        {
            var actual = _calculator.Add("11");
            var expected = 11;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_TwoNumbers_When_Adding_Then_ReturnTheirSum()
        {
            var actual = _calculator.Add("22,2");
            var expected = 24;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_MultipleNumbers_When_Adding_Then_ReturnTheirSum()
        {
            var actual = _calculator.Add("5,2,10001,6");
            var expected = 13;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_NewLineAsDelimiter_When_Adding_Then_ReturnSum()
        {
            var actual = _calculator.Add("5\n36,7");
            var expected = 48;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_SingleCharacterDelimiter_When_Adding_ThenReturnSum()
        {
            var actual = _calculator.Add("//:\n32:147:63");
            var expected = 242;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_NegativeNumbers_When_Adding_Then_ThrowException()
        {
            var actual = Assert.Throws<Exception>(() => _calculator.Add("-63, -1"));
            var expect = "Negatives not allowed -63, -1";

            Assert.AreEqual(expect, actual.Message);
        }

        [Test]
        public void Given_NumbersBiggerThanAThousand_When_Adding_Then_IngoreThem()
        {
            var actual = _calculator.Add("//:\n32:147:1001");
            var expected = 179;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_SingleDelimiterWithMultipleCharacters_When_Adding_ReturnSum()
        {
            var actual = _calculator.Add("//:::::\n45:::::2:::::45:::::30");
            var expected = 122;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_MultipleDelimitersWithSingleCharacter_When_Adding_Then_ReturnSum()
        {
            var actual = _calculator.Add("//[:][%]\n45:2%45%30:67");
            var expected = 189;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_MultipleDelimitersWithMultipleCharacters_When_Adding_ReturnSum()
        {
            var actual = _calculator.Add("//[:::::+][+++]\n45:::::+2:::::+45:::::+30+++67");
            var expected = 189;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Given_MultipleDelimitersBadString_When_Adding_ReturnSum()
        {
            var actual = Assert.Throws<Exception>(() => _calculator.Add("//[$$][$@][$][@@]\n1$#2$$3@@5$@10"));
            var expected = "Bad string";

            Assert.AreEqual(expected, actual.Message);
        }
    }
}
